# Setup
### Dependencies
 - [node js](https://nodejs.org)

After you've cloned the repository, run the following command:
 - `npm i` - install required node modules

### Building
 - `npm run build` - builds the site with the production config
 - `npm run watch` - rebuilds the site with dev config everytime a change is made to in the src folder
 - `npm run debug` - runs the watch script with DEBUG set to true

### Recommended workflow
 - open dist/index.html in your browser
 - `npm run watch` and make changes in the src/

### Babel/ES7+
This repo uses [Babel](https://github.com/babel/babel) with additional plugins for access to cutting-edge javascript features such as [pipeline operator](https://github.com/babel/babel/tree/master/packages/babel-plugin-proposal-pipeline-operator), [class properties](https://github.com/babel/babel/tree/master/packages/babel-plugin-proposal-class-properties), [decorators](https://github.com/babel/babel/tree/master/packages/babel-plugin-proposal-decorators), [optional chaining](https://github.com/babel/babel/tree/master/packages/babel-plugin-proposal-optional-chaining), and [nullish coalescing operator](https://github.com/babel/babel/tree/master/packages/babel-plugin-proposal-nullish-coalescing-operator).

### Repository Structure
 - `src` - contains the source with app.js as the entry point
 - `dist` - output of the site
