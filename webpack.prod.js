const path = require('path');
const webpack = require('webpack');

module.exports = {
	mode: 'production',
	entry: {
		app: './src/app.js'
	},
	output: {
		filename: 'iconcreator.[name].js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules: [
			{ test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
			{
				test: /\.css$/, use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader', options: { importLoaders: 1 } },
					{ loader: 'postcss-loader' }
				]
			},
			{ test: /\.html$/, loader: 'html-loader' },
			{ test: /\.frag$/, loader: 'raw-loader' },
			{ test: /\.vert$/, loader: 'raw-loader' }
		]
	},
	plugins: [
		new webpack.DefinePlugin(process.env.COMMIT_REF ? {
			DEBUG: false,
			__built_at__: JSON.stringify(new Date().toString()),
			__netlify__: JSON.stringify(`commit:${process.env.COMMIT_REF}`)
		} : {
			DEBUG: false,
			__built_at__: JSON.stringify(new Date().toString())
		}),
		new webpack.ExtendedAPIPlugin()
	],
	externals: [
		{ "pixi.js": "PIXI" },
		{ "sweetalert2": "swal" },
		{ "jquery": "jQuery" },
		{ "selectize": "Selectize" },
		{ "dropzone": "Dropzone" }
	],
	stats: {
		colors: true
	}
};
