import * as PIXI from 'pixi.js';
import defaultVertexSrc from './default.vert';
import { app } from '../app';

/**
 * Base class for creating shaders for the icon's background.
 * @extends {PIXI.Shader}
 */
export class BaseBackgroundShader extends PIXI.Shader {

	/**
	 * Base Background Shader contructor
	 * @param {string} fragmentSrc The source of the fragment shader.
	 * @param {Object} uniforms Custom uniforms to use to augment the built-in ones.
	 * @param {Object.<string, PropertyController>} controllers key = label  value = PropertyControllers
	 */
	constructor(fragmentSrc, uniforms, controllers) {

		super(PIXI.Program.from(defaultVertexSrc, fragmentSrc), uniforms);

		this.controllers = controllers;
	}

	loadControllers(container) {

		// clear previous property inputs
		container.empty();

		// // clear previous instances of colpick from DOM
		// $('.colpick').remove();

		// load all of this shader's properties
		Object.keys(this.controllers).forEach(p => {
			container.append(this.controllers[p].load(this, p));
		});
	}

	redraw() {
		app.ticker.start();
	}
}

/**
 * Shader display name for UI
 * @param {string} name Humanized name
 */
export function DisplayName(name) {
	return function decorator(target) {
		target.displayName = name;
	}
}
